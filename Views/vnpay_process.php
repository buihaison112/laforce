<?php
require_once './Models/Cart.php';
require_once './Core/Mailer.php';
$cartModel = new Cart();
$createCustomer = $cartModel->insertCustomer($name, $email, $phone, $address);
$idCustomer = $cartModel->returnLastId();
$createOrder = $cartModel->insertOrder($idOrder, $idCustomer, $note, $payment);
if(isset($_SESSION['cart']) || !empty($_SESSION['cart'])) {
    foreach($_SESSION['cart'] as $id) {
        foreach($id as $product) {
            $total = $product['qty'] * $product['price'];
            $createOrderDetail = $cartModel->insertOrderDetail($idOrder, $product['id'], $product['name'], $product['size'], $product['price'], $product['qty'], $total);
        }
    }
    $content = contentMailForMany($idOrder, $name, $email, $phone, $address, $payment);
    $title = "Bạn đã mua hàng thành công tại Laforce";
}
if($createOrderDetail) {
    // Gửi mail
    $mail = new Mailer();
    $mail -> sendMail($email, $title, $name, $content);
    $_SESSION['info'] = [$name, $address, $phone, $email, $payment];
    header('location: thanh-cong');
}
