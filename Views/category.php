    <!-- banner -->
    <?php
    include_once './public/banner.php';
    ?>

    <!-- list-product shoes -->
    <section class="box-list-product mb-15">
        <div class="container">
            <div class="title-boc-cart-list">
                <span>Giày da nam</span>
            </div>
            <div class="list-product row">
                <!-- 1 -->
                <div class="col-md-6">
                    <div class="box-content">
                        <a href="san-pham/giay-tay/1">
                            <div class="img-product-page">
                                <img src="./image/product/product-shoes (1).png" alt="">
                            </div>
                            <div class="cate-list-title">
                                <p>Giày tây nam</p>
                                <span><?=$countWestern?> sản phẩm</span>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- 2 -->
                <div class="col-md-6">
                    <div class="box-content">
                        <a href="san-pham/giay-luoi/1">
                            <div class="img-product-page">
                                <img src="./image/product/product-shoes (2).png" alt="">
                            </div>
                            <div class="cate-list-title">
                                <p>Giày lười nam</p>
                                <span><?=$countLazy?> sản phẩm</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
