<?php
session_start();
ob_start();
include_once '../../helper/helper.php';
if(isset($_FILES['avatar'])) {
    $filesAva = $_FILES['avatar'];
    $fileName = $_FILES['avatar']['name'];
    // validate file
    $flag = true;
    $fileType = ['png', 'jpg', 'jpeg'];
    // Lam the nao de chi upload hinh anh jpg, jpeg, png, ...
    $myFileType = strtolower(pathinfo($fileName, PATHINFO_EXTENSION)); //duoi file: jpg, png, ...
    if(!in_array($myFileType, $fileType)) {
        $flag = false;
    }
    // Lam the na de gioi han dung luong upload
    if($_FILES['avatar']['size'] >= 1000000000) {
        $flag = false;
    }
    if($flag== true) {
        $nameFileAva = converSlugUrl($filesAva['name']);
        $nameFileAva = rand().$nameFileAva;
        $tmpFileAva = $filesAva['tmp_name'];
        $flag = true;
        if (move_uploaded_file($tmpFileAva, './upload/'.$nameFileAva)){
            $_SESSION['ava'] = $nameFileAva;
            echo $nameFileAva;
        }
        else {
            echo 0;
        }
    }
    else {
        echo 0;
    }
}

if(isset($_FILES['sub_avatar'])) {
    // $_SESSION['subAva'] = [];
    
    $fileSubAva = $_FILES['sub_avatar'];
    if(!empty($fileSubAva['name']) && count($fileSubAva['name']) >= 1) {
        for ($i = 0; $i < count($fileSubAva['name']); $i++) {
            if(checkFile($fileSubAva['name'][$i], $fileSubAva['size'][$i])) {
                $len = isset($_SESSION['subAva']) ? count($_SESSION['subAva']) : 0;
                $nameFileSub = "sub".time().rand().$fileSubAva['name'][$i];
                $nameFileSub = converSlugUrl($nameFileSub);
                $tmpFileSub = $fileSubAva['tmp_name'][$i];
                $_SESSION['subAva'][$len+1] = $nameFileSub;
                if(move_uploaded_file($tmpFileSub, './upload/'.$nameFileSub)){
                    ?>
                        <img id="sub_img__<?=$nameFileSub?>" class="m-t-10 m-r-10 up_subava__success" style="width: 100px;" src="./Servers/upload/<?=$nameFileSub?>" alt="">
                    <?php
                }
            }
        }
    }
}
