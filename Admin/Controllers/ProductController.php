<?php
include_once './Core/Controller.php';
require_once './vendor/autoload.php';

use Carbon\Carbon;
class ProductController extends Controller
{
    public $productModel;
    public $categoryModel;
    public $sizeModel;
    public $storeModel;
    public function __construct()
    {
        $this->productModel = parent::model('Product');
        $this->categoryModel = parent::model('Category');
        $this->sizeModel = parent::model('Size');
        $this->storeModel = parent::model('Store');
        
        $this->index();
    }

    public function index ()
    {
        $method = isset($_GET['method']) ? $_GET['method'] : 'show';
        switch($method) {
            case 'show':
                $now = Carbon::now();
                $toDay = $now->format('Y-m-d');
                $firstOfMonth = $now->firstOfMonth()->format('Y-m-d');

                $nowSub = Carbon::now();
                $toDaySub = $nowSub->format('m-d-Y');
                $firstOfMonthSub = $nowSub->firstOfMonth()->format('m-d-Y');
                // Hiện thị thông tin sản phẩm
                $products = $this->productModel->showProduct();

                include_once './views/product/show_product.php';
                break;
            case 'add':
                $categories = $this->categoryModel->showCategory();
                include_once './views/product/add_product.php';
                $this->addProduct();
                break;
            case 'delete':
                $this->deleteProduct();
                break;
            case 'more':
                $this->moreProduct();
                break;
            case 'update':
                include_once './views/product/update_product.php';
                break;
            case 'edit':
                $this->editProduct();
                break;
        }
    }

    public function checkFile($fileName, $fileSize)
    {
        // validate file
        $flag = true;
        $fileType = ['png', 'jpg', 'jpeg'];
        // Lam the nao de chi upload hinh anh jpg, jpeg, png, ...
        $myFileType = strtolower(pathinfo($fileName, PATHINFO_EXTENSION)); //duoi file: jpg, png, ...
        if(!in_array($myFileType, $fileType)) {
            $flag = false;
            // $_SESSION['errors'] = 'Tải file không hợp lệ!';
        }
        // Lam the na de gioi han dung luong upload
        if($fileSize >= 1000000000) {
            $flag = false;
            // $_SESSION['errors'] = 'Dung lượng file quá lớn!';
        }
        return $flag;
    }

    public function addProduct()
    {
        if(isset($_POST['add_product'])) {
            $errors = [];
            $categoryId = $_POST['title'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $description = $_POST['description'];
            $filesAva = $_FILES['avatar'];
            $flag = false;
            if(checkFile($filesAva['name'], $filesAva['size'])) {
                $nameFileAva = $_SESSION['ava'];
                $tmpFileAva = $filesAva['tmp_name'];
                $flag = true;
                move_uploaded_file($tmpFileAva, '../store_img/'.$nameFileAva);
                $addProduct = $this->productModel->createProduct($categoryId, $name, $price, $nameFileAva, $description);
                unlink('./Servers/upload/'.$nameFileAva);
            }
            $productId = $this->productModel->returnLastId();

            $fileSubAva = $_FILES['sub_avatar'];
            if(!empty($fileSubAva['name']) && count($fileSubAva['name']) > 1) {
                for ($i = 0; $i < count($fileSubAva['name']); $i++) {
                    if(checkFile($fileSubAva['name'][$i], $fileSubAva['size'][$i])) {
                        $nameFileSub = $_SESSION['subAva'][$i];
                        $tmpFileSub = $fileSubAva['tmp_name'][$i];
                        move_uploaded_file($tmpFileSub, '../store_img/'.$nameFileSub);
                        $addListImg = $this->productModel->insertSubAvatar($productId, $nameFileSub);
                        unlink('./Servers/upload/'.$nameFileSub);
                    }
                }
           }
            if(isset($addProduct) && isset($addListImg) && $addProduct && $addListImg) {
                unlink($_SESSION['ava']);
                unlink($_SESSION['subAva']);
                $_SESSION['alert'] = 1;
                header('location: index.php?page=product');
            }
            else {
                $_SESSION['errors'] = 1;
                $delProduct = $this->productModel->deleteById($productId);
                header('location: index.php?page=product&method=add');
            }
        }
        else {
            //xoa file o muc upload khi ko submit form tai anh san pham

            if(isset($_SESSION['subAva']) || isset($_SESSION['ava'])) {
                unlink('./Servers/upload/'.$_SESSION['ava']);
                $len = count($_SESSION['subAva']);
                for ($i = 1; $i <= $len; $i++) {
                    unlink('./Servers/upload/'.$_SESSION['subAva'][$i]);
                }
                unset($_SESSION['subAva']);
                unset($_SESSION['ava']);    
            }
        }
    }
    public function editProduct()
    {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $product = $this->productModel->showProductById($id);
            $listImg = $this->productModel->showImgProductById($id);
            $sizes  = $this->sizeModel->showSize();
            $product['list_image'] = $listImg;
            $categories = $this->categoryModel->showCategory();

            if(isset($_POST['edit_product'])) {
                // update table products
                $categoryId = $_POST['title'];
                $name = $_POST['name'];
                $price = $_POST['price'];
                $description = $_POST['description'];
                $active = $_POST['active'];

                $nameFileAva = $oldFile = $product['avatar'];
                $filesAva = $_FILES['avatar'];

                // update file avatar
                if (!empty($filesAva['name'])) {
                    $nameFileAva = $_SESSION['ava'];
                    $tmpFileAva = $filesAva['tmp_name'];
                    move_uploaded_file($tmpFileAva, '../store_img/'.$nameFileAva);
                    unlink('../store_img/'.$oldFile);
                    unlink('./Servers/upload/'.$nameFileAva);
                }
                $update = $this->productModel->updateById($id, $categoryId, $name, $price, $nameFileAva, $description, $active);
                
                // update size and quantity to store
                $sizePosts = $_POST['size'];
                $quantity = $_POST['quantity'];
                if($quantity > 0) {
                    foreach($sizePosts as $size) {
                        $insertStore = $this->storeModel->addStore($id, $quantity, $size);
                    }  
                }

                // Update file sub avatar
                if(!empty($_POST['sub_avatar__id'])) {
                    $postSubAva = $_POST['sub_avatar__id'];
                    $subConvert = explode(',',$postSubAva);
                    $delSubAva = $this->productModel->updateSubAvaById($subConvert); //Xóa mềm set active về 0
                }
                
                // Thêm file sub avatar
                $fileSubAva = $_FILES['sub_avatar'];
                if(!empty($fileSubAva['name']) && count($fileSubAva['name']) >= 1) {
                    for ($i = 0; $i < count($fileSubAva['name']); $i++) {
                        if(checkFile($fileSubAva['name'][$i], $fileSubAva['size'][$i])) {
                            $nameFileSub = $_SESSION['subAva'][$i+1];
                            $tmpFileSub = $fileSubAva['tmp_name'][$i];
                            move_uploaded_file($tmpFileSub, '../store_img/'.$nameFileSub);
                            $addListImg = $this->productModel->insertSubAvatar($id, $nameFileSub);
                            unlink('./Servers/upload/'.$nameFileSub);
                        }
                    }
                }
                
                if($update ||  $delSubAva || $insertStore || $addListImg) { //cap nhat san pham thanh cong
                    unset($_SESSION['ava']);
                    unset($_SESSION['subAva']);
                    $_SESSION['alert'] = 3;
                    header('location: index.php?page=product&method=edit&id='.$id);
                }
            }
            else {
                //xoa file o muc upload khi ko submit form tai anh san pham
                if(isset($_SESSION['ava'])) {
                    unlink('./Servers/upload/'.$_SESSION['ava']);
                    unset($_SESSION['ava']);
                } 
                if (isset($_SESSION['subAva'])) {
                    $len = count($_SESSION['subAva']);
                    for ($i = 1; $i <= $len; $i++) {
                        unlink('./Servers/upload/'.$_SESSION['subAva'][$i]);
                    }
                    unset($_SESSION['subAva']);
                }
                
            }
            include_once './views/product/edit_product.php';
        }
    }

    public function deleteProduct() {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $product = $this->productModel->showProductById($id);
            unlink('../store_img/'.$product['avatar']);
            $delProduct = $this->productModel->deleteById($id);
            $listImg = $this->productModel->showImgProductById($id);
            foreach($listImg as $img) {
                unlink('../store_img/'.$img['path']);
            }
            $delImg = $this->productModel->deleteSubById($id);
            if($delProduct && $delImg) {
                $_SESSION['alert'] = 2;
                header('location: index.php?page=product');
            }
        }
    }

    public function moreProduct() {

    }
}