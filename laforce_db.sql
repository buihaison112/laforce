-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 20, 2022 lúc 05:23 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `laforce_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bills`
--

CREATE TABLE `bills` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `code` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `bills`
--

INSERT INTO `bills` (`id`, `order_id`, `name`, `total`, `code`, `created_at`, `status`) VALUES
(2244, 7331, 'Đoàn Đắc Tùng', '3300000', 8613, '2022-10-17 15:35:08', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `active` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `title`, `create_at`, `updated_at`, `status`, `active`) VALUES
(1, 'Giày tây nam', '2022-09-01 15:19:51', '2022-09-01 15:19:51', 1, 1),
(2, 'Giày lười nam', '2022-09-01 15:20:00', '2022-09-01 15:20:00', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` char(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `address`, `status`, `created_at`) VALUES
(63, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-12 10:11:57'),
(64, 'ĐInh Hà', 'toannd158@gmail.com', '0986204681', 'Nam Định', 1, '2022-09-12 10:12:55'),
(65, 'Nguyễn Hoàng Anh', 'toannd158@gmail.com', '0868642605', 'Ứng Hòa, Hà Nội', 1, '2022-09-25 17:38:57'),
(66, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-25 17:44:09'),
(67, 'Doan Ngoc Toan', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 10:46:11'),
(68, 'Doan Ngoc Toan', 'toannd158@gmail.com', '0868642605', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 11:07:03'),
(69, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-26 11:44:53'),
(70, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-26 11:46:28'),
(71, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 11:59:10'),
(72, 'Doan Ngoc Toan', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 12:59:05'),
(73, 'Ninh Thi Thuy Hang', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:38:24'),
(74, 'Phan Đức Trọng', 'toannd158@gmail.com', '0868642605', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:47:19'),
(75, 'Ninh Thi Thuy Hang', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 16:49:30'),
(76, 'Ninh Thi Thuy Hang', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 16:52:09'),
(77, 'Nguyễn Mai Anh', 'toannd158@gmail.com', '0868642605', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:53:26'),
(78, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0986204681', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:54:15'),
(79, 'Doan Ngoc Toan', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:56:45'),
(80, 'Doan Ngoc Hang', 'toannd158@gmail.com', '0868642605', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 16:57:35'),
(81, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 17:01:48'),
(82, 'Doan Ngoc Hang', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-26 17:08:17'),
(83, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 23:25:29'),
(84, 'Phan Đức Trọng', 'toannd158@gmail.com', '0986204681', 'Kiến Thụy, Hải Phòng', 1, '2022-09-26 23:37:33'),
(85, 'Nguyễn Hoàng Anh', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-26 23:39:37'),
(86, 'Ninh Thi Thuy Hang', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-28 20:54:09'),
(87, 'Trần Ngọc Sơn', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-30 22:42:53'),
(88, 'Trần Ngọc Sơn', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-30 22:43:44'),
(89, 'Doan Ngoc Hang', 'Spadmin@gmail.com', '0986204681', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-30 22:52:04'),
(90, 'Doan Ngoc Toan', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-30 22:57:52'),
(91, 'Bùi Hải Sơn', 'toannd158@gmail.com', '0868642605', 'Mai Dich, Cau Giay, Ha Noi', 1, '2022-09-30 23:12:06'),
(92, 'Hoàng Mạnh Tùng', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-30 23:12:33'),
(93, 'Ninh Thị Thúy Hằng', 'toannd158@gmail.com', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-09-30 23:59:05'),
(94, 'Đoàn Đắc Tùng', 'toannd158@gmail.com123', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-10-03 10:01:43'),
(95, 'Đoàn Đắc Tùng', 'toannd158@gmail.com123', '0868642605', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-10-03 10:01:48'),
(96, 'Trần Hoài Nam', 'toannd158@gmail.com', '0986204681', 'Tân Trào, Kiến Thụy, Hải Phòng', 1, '2022-10-03 10:03:54'),
(97, 'Đoàn Đắc Tùng', 'toannd158@gmail.com', '0868642605', 'Tỉnh Cao Bằng, Huyện Bảo Lâm, Xã Thạch Lâm, số nhà 31', 1, '2022-10-17 15:33:44'),
(98, 'Đoàn Ngọc Toàn', 'toannd158@gmail.com', '0986204681', 'thôn a, Xã Bản Hon, Huyện Tam Đường, Tỉnh Lai Châu', 1, '2022-10-17 16:41:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `rate` tinyint(1) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback_img`
--

CREATE TABLE `feedback_img` (
  `id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_image`
--

CREATE TABLE `list_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `list_image`
--

INSERT INTO `list_image` (`id`, `product_id`, `path`, `created_at`, `updated_at`, `active`) VALUES
(106, 12, 'sub16621980291995237768product-3-1.jpg', '2022-09-03 16:40:29', '2022-09-03 16:40:29', 1),
(107, 12, 'sub16621980292088136076product-3-2.jpg', '2022-09-03 16:40:29', '2022-09-03 16:40:29', 1),
(108, 12, 'sub1662198029256350543product-3-3.jpg', '2022-09-03 16:40:29', '2022-09-03 16:40:29', 1),
(109, 12, 'sub1662198029511900137product-3-4.jpg', '2022-09-03 16:40:29', '2022-09-03 16:40:29', 1),
(110, 13, 'sub16621980421040980303product-4-1.jpg', '2022-09-03 16:40:42', '2022-09-03 16:40:42', 1),
(111, 13, 'sub16621980422017263785product-4-2.jpg', '2022-09-03 16:40:42', '2022-09-03 16:40:42', 1),
(112, 13, 'sub16621980421013345528product-4-3.jpg', '2022-09-03 16:40:42', '2022-09-03 16:40:42', 1),
(113, 13, 'sub1662198042202191411product-4-4.jpg', '2022-09-03 16:40:42', '2022-09-03 16:40:42', 1),
(153, 73, 'sub16626040691210447520product-5-1.jpg', '2022-09-08 09:27:49', '2022-09-08 09:27:49', 1),
(154, 73, 'sub16626040691795855413product-5-2.jpg', '2022-09-08 09:27:49', '2022-09-08 09:27:49', 1),
(155, 73, 'sub16626040691021954553product-5-3.jpg', '2022-09-08 09:27:49', '2022-09-08 09:27:49', 1),
(156, 73, 'sub166260406987034278product-5-4.jpg', '2022-09-08 09:27:49', '2022-09-08 09:27:49', 1),
(157, 74, 'sub16626041351531117564product-7-1.jpg', '2022-09-08 09:28:55', '2022-09-08 09:28:55', 1),
(158, 74, 'sub16626041351893775974product-7-2.jpg', '2022-09-08 09:28:55', '2022-09-08 09:28:55', 1),
(159, 74, 'sub16626041351720202861product-7-3.jpg', '2022-09-08 09:28:55', '2022-09-08 09:28:55', 1),
(160, 74, 'sub1662604135113779205product-7-4.jpg', '2022-09-08 09:28:55', '2022-09-08 09:28:55', 1),
(161, 75, 'sub16626041831788487737product-8-1.jpg', '2022-09-08 09:29:43', '2022-09-08 09:29:43', 1),
(162, 75, 'sub16626041831306167908product-8-2.jpg', '2022-09-08 09:29:43', '2022-09-08 09:29:43', 1),
(163, 75, 'sub16626041831944295257product-8-3.jpg', '2022-09-08 09:29:43', '2022-09-08 09:29:43', 1),
(164, 75, 'sub1662604183452456078product-8-4.jpg', '2022-09-08 09:29:43', '2022-09-08 09:29:43', 1),
(169, 77, 'sub1662947518916685735product-9-0.jpg', '2022-09-12 08:51:58', '2022-09-12 08:51:58', 1),
(170, 77, 'sub16629475181341246190product-9-2.jpg', '2022-09-12 08:51:58', '2022-09-12 08:51:58', 1),
(171, 77, 'sub1662947518215917429product-9-3.jpg', '2022-09-12 08:51:58', '2022-09-12 08:51:58', 1),
(172, 77, 'sub16629475181947709456product-9-4.jpg', '2022-09-12 08:51:58', '2022-09-12 08:51:58', 1),
(173, 78, 'sub16629476311345035217product-10-1.jpg', '2022-09-12 08:53:51', '2022-09-12 08:53:51', 1),
(175, 78, 'sub1662947631242526743product-10-3.jpg', '2022-09-12 08:53:51', '2022-09-12 08:53:51', 1),
(176, 78, 'sub16629476311462925561product-10-4.jpg', '2022-09-12 08:53:51', '2022-09-12 08:53:51', 1),
(177, 79, 'sub1662947743106169084product-11-1.jpg', '2022-09-12 08:55:43', '2022-09-12 08:55:43', 1),
(178, 79, 'sub1662947743418834984product-11-2.jpg', '2022-09-12 08:55:43', '2022-09-12 08:55:43', 1),
(179, 79, 'sub1662947743410738447product-11-3.jpg', '2022-09-12 08:55:43', '2022-09-12 08:55:43', 1),
(180, 79, 'sub1662947743852737005product-11-4.jpg', '2022-09-12 08:55:43', '2022-09-12 08:55:43', 1),
(181, 80, 'sub166294784698631577product-12-1.jpg', '2022-09-12 08:57:26', '2022-09-12 08:57:26', 1),
(182, 80, 'sub1662947846870912476product-12-2.jpg', '2022-09-12 08:57:26', '2022-09-12 08:57:26', 1),
(183, 80, 'sub16629478461235223334product-12-3.jpg', '2022-09-12 08:57:26', '2022-09-12 08:57:26', 1),
(184, 80, 'sub1662947846918256740product-12-4.jpg', '2022-09-12 08:57:26', '2022-09-12 08:57:26', 1),
(185, 81, 'sub1662947972922024036product-13-1.jpg', '2022-09-12 08:59:32', '2022-09-12 08:59:32', 1),
(186, 81, 'sub16629479722019638018product-13-2.jpg', '2022-09-12 08:59:32', '2022-09-12 08:59:32', 1),
(187, 81, 'sub16629479722141072583product-13-3.jpg', '2022-09-12 08:59:32', '2022-09-12 08:59:32', 1),
(188, 81, 'sub166294797227602104product-13-4.jpg', '2022-09-12 08:59:32', '2022-09-12 08:59:32', 1),
(189, 82, 'sub166294811656697788product-14-1.jpg', '2022-09-12 09:01:56', '2022-09-12 09:01:56', 1),
(190, 82, 'sub16629481161096384983product-14-2.jpg', '2022-09-12 09:01:56', '2022-09-12 09:01:56', 1),
(191, 82, 'sub1662948116318820109product-14-3.jpg', '2022-09-12 09:01:56', '2022-09-12 09:01:56', 1),
(192, 82, 'sub16629481161749649051product-14-4.jpg', '2022-09-12 09:01:56', '2022-09-12 09:01:56', 1),
(193, 83, 'sub16632287521730029558product-1-1.jpg', '2022-09-15 14:59:12', '2022-09-15 14:59:12', 1),
(194, 83, 'sub1663228752388677562product-1-2.jpg', '2022-09-15 14:59:12', '2022-09-15 14:59:12', 1),
(195, 83, 'sub16632287531486130917product-1-3.jpg', '2022-09-15 14:59:13', '2022-09-15 14:59:13', 1),
(196, 83, 'sub1663228753151634388product-1-4.jpg', '2022-09-15 14:59:13', '2022-09-15 14:59:13', 1),
(197, 84, 'sub16632288401976679517product-2-1.jpg', '2022-09-15 15:00:40', '2022-09-15 15:00:40', 1),
(198, 84, 'sub1663228840925102054product-2-2.jpg', '2022-09-15 15:00:40', '2022-09-15 15:00:40', 1),
(199, 84, 'sub1663228841927458709product-2-3.jpg', '2022-09-15 15:00:41', '2022-09-15 15:00:41', 1),
(200, 84, 'sub16632288412145352657product-2-4.jpg', '2022-09-15 15:00:41', '2022-09-15 15:00:41', 1),
(205, 87, 'sub16638132221573829147gnla0819-n-2.jpg', '2022-09-22 09:20:22', '2022-09-22 09:20:22', 1),
(206, 87, 'sub16638132222004441601product-1.jpg', '2022-09-22 09:20:22', '2022-09-22 09:20:22', 1),
(207, 87, 'sub1663813222617532566product-15-3.jpg', '2022-09-22 09:20:22', '2022-09-22 09:20:22', 1),
(208, 87, 'sub16638132222139587121product-15-4.jpg', '2022-09-22 09:20:22', '2022-09-22 09:20:22', 1),
(209, 88, 'sub16638133622010038565gnla19-5-d-1.jpg', '2022-09-22 09:22:42', '2022-09-22 09:22:42', 1),
(210, 88, 'sub16638133621350273546gnla19-5-d-2.jpg', '2022-09-22 09:22:42', '2022-09-22 09:22:42', 1),
(211, 88, 'sub16638133621309757138gnla19-5-d-3.jpg', '2022-09-22 09:22:42', '2022-09-22 09:22:42', 1),
(212, 88, 'sub1663813362641639974gnla19-5-d-5.jpg', '2022-09-22 09:22:42', '2022-09-22 09:22:42', 1),
(213, 89, 'sub1663813493385468819giay-nam-penny-loafer-gnla8246-d-1-1.jpg', '2022-09-22 09:24:53', '2022-09-22 09:24:53', 1),
(214, 89, 'sub1663813493135174267giay-nam-penny-loafer-gnla8246-d-1-2.jpg', '2022-09-22 09:24:53', '2022-09-22 09:24:53', 1),
(215, 89, 'sub1663813493549613467giay-nam-penny-loafer-gnla8246-d-2-2.jpg', '2022-09-22 09:24:53', '2022-09-22 09:24:53', 1),
(216, 89, 'sub16638134931674567061gnla8246-d-1.jpg', '2022-09-22 09:24:53', '2022-09-22 09:24:53', 1),
(217, 90, 'sub16638137461223916505gnla55298-3-n-1.jpg', '2022-09-22 09:29:06', '2022-09-22 09:29:06', 1),
(218, 90, 'sub16638137461625127798gnla55298-3-n-3.jpg', '2022-09-22 09:29:06', '2022-09-22 09:29:06', 1),
(219, 90, 'sub16638137461064794252gnla55298-3-n-4.jpg', '2022-09-22 09:29:06', '2022-09-22 09:29:06', 1),
(220, 90, 'sub1663813746202355282gnla55298-3-n-5.jpg', '2022-09-22 09:29:06', '2022-09-22 09:29:06', 1),
(221, 91, 'sub16638138541815767830giay-tay-nam-phoi-vien-gnla101-20-n-1-1.jpg', '2022-09-22 09:30:54', '2022-09-22 09:30:54', 1),
(222, 91, 'sub16638138541591565831giay-tay-nam-phoi-vien-gnla101-20-n-3.jpg', '2022-09-22 09:30:54', '2022-09-22 09:30:54', 1),
(223, 91, 'sub16638138541998531697giay-tay-nam-phoi-vien-gnla101-20-n-4.jpg', '2022-09-22 09:30:54', '2022-09-22 09:30:54', 1),
(224, 91, 'sub1663813854865497177gnla101-20-n-1.jpg', '2022-09-22 09:30:54', '2022-09-22 09:30:54', 1),
(225, 92, 'sub1663814056695348755giay-tay-spectator-hai-mau-da-gnla81711-05-nv-1.jpg', '2022-09-22 09:34:16', '2022-09-22 09:34:16', 1),
(226, 92, 'sub1663814056163424725giay-tay-spectator-hai-mau-da-gnla81711-05-nv-3.jpg', '2022-09-22 09:34:16', '2022-09-22 09:34:16', 1),
(227, 92, 'sub16638140561162263780gnla81711-05-nv-1.jpg', '2022-09-22 09:34:16', '2022-09-22 09:34:16', 1),
(228, 92, 'sub1663814057224773687gnla81711-05-nv-2--1-.jpg', '2022-09-22 09:34:17', '2022-09-22 09:34:17', 1),
(229, 93, 'sub16638141521050328162giay-boots-nam-da-bo-gnla36700-r58-xn-1-1.jpg', '2022-09-22 09:35:52', '2022-09-22 09:35:52', 1),
(230, 93, 'sub16638141521975052796giay-boots-nam-da-bo-gnla36700-r58-xn-2.jpg', '2022-09-22 09:35:52', '2022-09-22 09:35:52', 1),
(231, 93, 'sub16638141522011881481giay-boots-nam-da-bo-gnla36700-r58-xn-2-1.jpg', '2022-09-22 09:35:52', '2022-09-22 09:35:52', 1),
(232, 93, 'sub166381415269734235giay-boots-nam-da-bo-gnla36700-r58-xn-4.jpg', '2022-09-22 09:35:52', '2022-09-22 09:35:52', 1),
(233, 94, 'sub16638142311763064908gnlamjdp30-17-cf-1.jpg', '2022-09-22 09:37:11', '2022-09-22 09:37:11', 1),
(234, 94, 'sub16638142311947238942gnlamjdp30-17-cf-2.jpg', '2022-09-22 09:37:11', '2022-09-22 09:37:11', 1),
(235, 94, 'sub16638142311884002089gnlamjdp30-17-cf-3.jpg', '2022-09-22 09:37:11', '2022-09-22 09:37:11', 1),
(236, 94, 'sub1663814231248111457gnlamjdp30-17-cf-5.jpg', '2022-09-22 09:37:11', '2022-09-22 09:37:11', 1),
(241, 78, 'sub16658025931668696091untitled-1-01.jpg', '2022-10-15 09:56:33', '2022-10-15 09:56:33', 0),
(242, 78, 'sub16658025931032491691untitled-1-02.jpg', '2022-10-15 09:56:33', '2022-10-15 09:56:33', 0),
(243, 78, 'sub16658027231114464129untitled-1-01.jpg', '2022-10-15 09:58:43', '2022-10-15 09:58:43', 0),
(244, 78, 'sub16658027231686114695untitled-1-02.jpg', '2022-10-15 09:58:43', '2022-10-15 09:58:43', 0),
(245, 95, 'sub16658030551108178006untitled-1-01.jpg', '2022-10-15 10:04:15', '2022-10-15 10:04:15', 0),
(246, 95, 'sub1665803055364489544untitled-1-02.jpg', '2022-10-15 10:04:15', '2022-10-15 10:04:15', 0),
(247, 96, 'sub1665979704949813305hang-xinh-gai.jpg', '2022-10-17 11:08:24', '2022-10-17 11:08:24', 1),
(248, 96, 'sub1665979704891167390use-case.png', '2022-10-17 11:08:24', '2022-10-17 11:08:24', 1),
(249, 97, 'sub16659798891148461320hang-xinh-gai.jpg', '2022-10-17 11:11:29', '2022-10-17 11:11:29', 1),
(250, 97, 'sub16659798901130108366use-case.png', '2022-10-17 11:11:30', '2022-10-17 11:11:30', 1),
(251, 98, 'sub1665979990670215951hang-xinh-gai.jpg', '2022-10-17 11:13:10', '2022-10-17 11:13:10', 1),
(252, 98, 'sub16659799901544292762use-case.png', '2022-10-17 11:13:10', '2022-10-17 11:13:10', 1),
(253, 95, 'sub16662774601255231872product-2-1.jpg', '2022-10-20 21:51:02', '2022-10-20 21:51:02', 0),
(254, 95, 'sub1666277460117260858product-2-2.jpg', '2022-10-20 21:51:02', '2022-10-20 21:51:02', 0),
(255, 95, 'sub1666277487365955115product-2-2.jpg', '2022-10-20 21:51:28', '2022-10-20 21:51:28', 0),
(256, 95, 'sub1666277487476855709product-2-3.jpg', '2022-10-20 21:51:28', '2022-10-20 21:51:28', 0),
(257, 95, 'sub16662775921722212381product-2-1.jpg', '2022-10-20 21:53:16', '2022-10-20 21:53:16', 0),
(258, 95, 'sub1666277592847771591product-2-2.jpg', '2022-10-20 21:53:16', '2022-10-20 21:53:16', 0),
(259, 95, 'sub16662776471584711823hang-xinh-gai.jpg', '2022-10-20 21:54:08', '2022-10-20 21:54:08', 0),
(260, 95, 'sub1666277647880794724use-case.png', '2022-10-20 21:54:08', '2022-10-20 21:54:08', 0),
(261, 95, 'sub166627867658374843untitled-1-01.jpg', '2022-10-20 22:11:18', '2022-10-20 22:11:18', 1),
(262, 95, 'sub1666278676691206210untitled-1-02.jpg', '2022-10-20 22:11:18', '2022-10-20 22:11:18', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `payment` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `note`, `status`, `payment`, `created_at`) VALUES
(7331, 97, '', 4, 'code', '2022-10-17 15:33:44'),
(7482, 98, '123', 2, 'code', '2022-10-17 16:41:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_size` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `product_name`, `product_size`, `price`, `quantity`, `total`, `status`) VALUES
(163, 7331, 83, 'Giầy nam họa tiết vân da rắn', 43, '1450000', 1, '1450000', 1),
(164, 7331, 91, 'Giày tây nam phối viền GNLA101-20-N', 42, '1850000', 1, '1850000', 1),
(165, 7482, 91, 'Giày tây nam phối viền GNLA101-20-N', 41, '1850000', 1, '1850000', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `active` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `price`, `avatar`, `created_at`, `updated_at`, `description`, `status`, `active`) VALUES
(12, 1, 'Giày da nam kiểu dáng Oxford', '1800000', '1662051729product-3-0.jpg', '2022-09-02 00:02:09', '2022-09-02 00:02:09', '<p><a href=\"https://laforce.vn/giay-da-nam-kieu-dang-oxford-gnlaaz01-1-d/\">Giày da nam kiểu dáng Oxford</a></p>', 1, 1),
(13, 1, 'Giày da nam vân da rắn nâu đỏ', '1300000', '1662051863product-4.jpg', '2022-09-02 00:04:23', '2022-09-02 00:04:23', '<p><a href=\"https://laforce.vn/giay-da-nam-van-ca-sau-mau-nau-gnlabc001-ndo/\">Giày da nam vân da rắn nâu đỏ</a></p>', 1, 1),
(73, 1, 'Giày Oxford nam phối màu nâu đỏ', '1450000', '1662604069product-5-0.jpg', '2022-09-08 09:27:49', '2022-09-08 09:27:49', '<p><strong>Giày Oxford nam phối màu nâu đỏ</strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&gt;</p><p>&gt;</p>', 1, 1),
(74, 2, 'Giày lười nam họa tiết kẻ ca rô', '1700000', '1662604135product-7-0.jpg', '2022-09-08 09:28:55', '2022-09-08 09:28:55', '<ul><li>Từng đường may kép tỉ mỉ, chắc chắn chạy quanh giày</li><li>Họa tiết đai da vắt ngang lưỡi giày cách điệu</li><li>Sản phẩm <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày mọi nam</strong></a> cách điệu với họa tiết nổi ở thân giày tạo cảm giác nam tính</li><li>Mũi giày tròn, bo viền chắc chắn</li><li>Màu: Đen</li></ul>', 1, 1),
(75, 2, 'Giày lười da nam GNLA2122-N', '1750000', '1662604183product-8-0.jpg', '2022-09-08 09:29:43', '2022-09-08 09:29:43', '<ul><li>Từng đường may kép tỉ mỉ, chắc chắn chạy quanh giày</li><li>Thiết kế&nbsp;<a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> họa tiết đan caro độc đáo tạo sự trẻ trung</li><li>Mũi giày tròn</li><li>Đế giày thiết kế chống trơn, trượt</li><li>Màu: Nâu</li></ul>', 1, 1),
(77, 1, 'Giày da Oxford Brogue GNLA9632-1301-D', '1750000', '1662947518product-9-1.jpg', '2022-09-12 08:51:58', '2022-09-12 08:51:58', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Kiểu dáng <a href=\"https://laforce.vn/giay-tay-nam/\"><strong>giày tây nam</strong></a> với màu sắc trang nhã, hài hòa.</li><li>Thiết kế hiện đại, sang trọng phù hợp với các quý ông lịch lãm.</li><li>Kết hợp cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Đen</li><li><strong>Kích thước: </strong>38 – 43</li></ul>', 1, 1),
(78, 1, 'Giày tây nam Oxford Brogues GNLA08-8-D', '2100000', '1662947631product-10-0.jpg', '2022-09-12 08:53:51', '2022-09-12 08:53:51', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Màu sắc trang nhã, hài hòa.</li><li>Phong cách <a href=\"https://laforce.vn/giay-oxford-nam/\"><strong>giày Oxford nam</strong></a> hiện đại, sang trọng phù hợp với các quý ông lịch lãm.</li><li>Kết hợp cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Đen</li><li><strong>Kích thước: </strong>38 – 43</li></ul><p>&gt;</p>', 1, 1),
(79, 1, 'Giày tây buộc dây Cap Toe Derby GNLA21021-N', '1950000', '1662947743product-11.jpg', '2022-09-12 08:55:43', '2022-09-12 08:55:43', '<ul><li>Chất liệu da bò nhập khẩu 100%, siêu bền đẹp</li><li>Phom giày mũi nhọn, viền hoạ tiết trên mũi giày</li><li>Đế xẻ rãnh chống trơn trượt</li><li>Màu: Nâu</li><li>Mẫu&nbsp;<a href=\"https://laforce.vn/giay-tay-nam/\"><strong>giày tây nam cao cấp</strong></a>&nbsp;độc quyền tại Đồ da LaForce</li></ul>', 1, 1),
(80, 2, 'Giày lười da nam Penny Loafer GNLA1199-D', '1150000', '1662947846product-12-0.jpg', '2022-09-12 08:57:26', '2022-09-12 08:57:26', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Thiết kế dạng Penny Loafer với đai da vắt ngang thân giày</li><li>Sản phẩm&nbsp;<a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a>&nbsp;thích hợp đi cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Đen</li><li><strong>Kích thước:&nbsp;</strong>38 – 43</li></ul>', 1, 1),
(81, 2, 'Giày nam Penny Loafer da lộn GNLA0828-N', '1600000', '1662947971product-13-0.jpg', '2022-09-12 08:59:31', '2022-09-12 08:59:31', '<ul><li><strong>Chất liệu</strong>: Da thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Mẫu <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> với màu sắc trang nhã, hài hòa.</li><li>Thiết kế hiện đại, sang trọng phù hợp với các quý ông lịch lãm.</li><li>Kết hợp cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Nâu&nbsp;</li><li><strong>Kích thước: </strong>38 – 43</li></ul>', 1, 1),
(82, 2, 'Giày lười nam Penny Loafer GNLA8878-102-D', '1600000', '1662948116product-14.jpg', '2022-09-12 09:01:56', '2022-09-12 09:01:56', '<ul><li>Từng đường may kép tỉ mỉ, chắc chắn chạy quanh giày</li><li>Họa tiết đai da vắt ngang lưỡi giày</li><li>Dáng <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> mũi tròn</li><li>Đế giày thiết kế chống trơn, trượt</li><li>Màu: Đen&nbsp;</li></ul>', 1, 1),
(83, 1, 'Giầy nam họa tiết vân da rắn', '1450000', '1663228752product-1-0.jpg', '2022-09-15 14:59:12', '2022-09-15 14:59:12', '<ul><li>Chất liệu da bò nhập khẩu 100%, siêu bền đẹp</li><li>Thiết kế <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> họa tiết giả vân da cá sấu sang trọng</li><li>Đường chỉ may tỉ mỉ theo tiêu chuẩn Châu Âu</li><li>Màu: Đen</li></ul><p>&gt;</p><p>&gt;</p>', 1, 1),
(84, 1, 'Giày da Oxford nam', '1200000', '1663228840product-2-0.jpg', '2022-09-15 15:00:40', '2022-09-15 15:00:40', '<ul><li>Thiết kế hiện đại</li><li>Kiểu dáng:&nbsp;<a href=\"https://laforce.vn/giay-oxford-nam/\"><strong>giày tây nam Oxford&nbsp;&nbsp;</strong></a></li><li>Đếxẻ rãnh chống trơn trượt</li><li>Mũi tròn hiện đại, dễ kết hợp trang phục</li><li>Đường chỉ may tỉ mỉ theo tiêu chuẩn châu Âu</li><li>Màu: nâu</li></ul><p>&gt;</p>', 1, 1),
(86, 1, 'Doan Ngoc Toan', '80000', '1665795922hang-xinh-gai.jpg', '2022-09-13 10:37:39', '2022-09-19 10:37:39', '', 1, -1),
(87, 1, 'Giày Oxford Brogues nâu GNLA0819-N', '1700000', '1663813222product-0.jpg', '2022-09-13 09:20:22', '2022-09-22 09:20:22', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Kiểu dáng oxford họa tiết brogues cổ điển sang trọng.</li><li>Kiểu dáng <a href=\"https://laforce.vn/giay-tay-nam/\"><strong>giày tây nam</strong></a> hiện đại, trang nhã phù hợp với các quý ông lịch lãm.</li><li>Kết hợp cùng quần âu, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Nâu</li><li><strong>Kích thước: </strong>38 – 43</li></ul><p>&gt;</p>', 1, 1),
(88, 2, 'Giày da Penny Loafer GNLA19-5-D', '1650000', '1663813362giay-da-luoi-nam-dai-ngang-gnla19-5-d-1-1.jpg', '2022-09-04 09:22:42', '2022-09-22 09:22:42', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Màu sắc trang nhã, hài hòa.</li><li>Thiết kế <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười da nam</strong></a> hiện đại, trẻ trung phù hợp với các quý ông năng động.</li><li>Kết hợp cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Đen</li><li><strong>Kích thước: </strong>38 – 43</li></ul><p>&gt;</p>', 1, 1),
(89, 2, 'Giày nam Penny Loafer GNLA8246-D', '1800000', '1663813493giay-nam-penny-loafer-gnla8246-d-1.jpg', '2022-09-13 09:24:53', '2022-09-22 09:24:53', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Màu sắc trang nhã, hài hòa.</li><li>Dáng <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> hiện đại, sang trọng phù hợp với các quý ông lịch lãm.</li><li>Kết hợp cùng quần âu, kaki, trang phục thanh lịch.&nbsp;</li><li><strong>Màu</strong>: Đen</li><li><strong>Kích thước: </strong>38 – 43</li></ul><p>&gt;</p>', 1, 1),
(90, 2, 'Giày nam giả vân da cá sấu viền xỏ dây GNLA55298-3-N', '1700000', '1663813746giay-nam-gia-van-da-ca-sau-vien-xo-day-gnla55298-3-n.jpg', '2022-09-15 09:29:06', '2022-09-22 09:29:06', '<ul><li>Từng đường may kép tỉ mỉ, chắc chắn chạy quanh giày</li><li>Thiết kế thắt dây chạy quanh miệng, thắt nơ đằng trước mũi giày</li><li><a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>Giày lười nam</strong></a> dáng mũi tròn hiện đại</li><li>Đế giày thiết kế chống trơn, trượt</li><li>Màu: Nâu</li><li>Chất liệu da bò nhập khẩu 100%</li><li>Da được xử lý theo đúng quy trình nên sử dụng càng lâu thì giày sẽ càng mềm mại, dẻo dai, bền màu và tăng độ bóng mịn</li></ul><p>&gt;</p>', 1, 1),
(91, 1, 'Giày tây nam phối viền GNLA101-20-N', '1850000', '1663813854giay-tay-nam-phoi-vien-gnla101-20-n.jpg', '2022-09-22 09:30:54', '2022-09-22 09:30:54', '<ul><li>Mũi giày tròn&nbsp;</li><li>Đường chỉ may tỉ mỉ, chắc chắn theo tiêu chuẩn xuất khẩu Châu Âu</li><li>Họa tiết đục lỗ tạo hoa văn độc đáo</li><li>Thiết kế thắt dây hiện đại</li><li>Mẫu <a href=\"https://laforce.vn/giay-tay-nam/\"><strong>giày tây nam</strong></a> với thiết kế đế chống trơn, trượt</li><li>Màu: Nâu vàng</li><li>Chất liệu da bò nhập khẩu 100%</li><li>Da được xử lý theo đúng quy trình nên sử dụng càng lâu thì giày sẽ càng mềm mại, dẻo dai, bền màu và tăng độ bóng mịn</li></ul>', 1, 1),
(92, 1, 'Giày tây Spectator hai màu da GNLA81711-05-NV', '2400000', '1663814056giay-tay-spectator-hai-mau-da-gnla81711-05-nv.jpg', '2022-09-13 09:34:16', '2022-09-22 09:34:16', '<ul><li>Đường chỉ may tỉ mỉ, chắc chắn theo tiêu chuẩn xuất khẩu Châu Âu</li><li>Kiểu dáng <a href=\"https://laforce.vn/giay-tay-nam/\"><strong>giày tây nam</strong></a> phối hợp hai màu da nâu, trắng cơ bản</li><li>Mũi giày tròn bo viền&nbsp;chắc chắn</li><li>Màu: Nâu vàng</li><li>Chất liệu da bò nhập khẩu 100%</li><li>Da được xử lý theo đúng quy trình nên sử dụng càng lâu thì giày sẽ càng mềm mại, dẻo dai, bền màu và tăng độ bóng mịn&nbsp;</li></ul><p>&gt;</p>', 1, 1),
(93, 1, 'Giày Boots nam da bò GNLA36700-R58-XN', '2900000', '1663814152giay-boots-nam-da-bo-gnla36700-r58-xn-1.jpg', '2022-09-13 09:35:52', '2022-09-22 09:35:52', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Thiết kế dáng boot cao cổ sành điệu</li><li>Sơn bóng với cách phối màu xanh, nâu hiện đại</li><li>Màu sắc trang nhã, hài hòa.</li><li>Kết hợp cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Nâu -&nbsp;</li><li><strong>Kích thước:&nbsp;</strong>38 – 43</li></ul><p>&nbsp;</p><ul><li>Chất liệu da bò nhập khẩu 100% từ châu Âu.</li><li>Da được xử lý theo đúng quy trình nên sử dụng càng lâu thì giày sẽ càng mềm mại, dẻo dai, bền màu và tăng độ bóng mịn.&nbsp;</li><li>Dễ dàng vệ sinh và bảo quản.</li></ul><p>&gt;</p><p>&gt;</p>', 1, 1),
(94, 1, 'Giày Monk Strap GNLAMJDP30-17-CF', '1800000', '1663814231giay-monk-strap-sang-trong-gnlamjdp30-17-cf-2.jpg', '2022-09-13 09:37:11', '2022-09-22 09:37:11', '<ul><li><strong>Chất liệu</strong>: Da bò thật toàn bộ từ châu Âu</li><li>Đường may chi tiết, tỉ mỉ theo tiêu chuẩn.</li><li>Đế giày chắc chắn, chống trơn trượt.</li><li>Màu sắc trang nhã, hài hòa.</li><li>Thiết kế hiện đại, sang trọng phù hợp với các quý ông lịch lãm.</li><li>Sản phẩm <a href=\"https://laforce.vn/giay-luoi-nam/\"><strong>giày lười nam</strong></a> thích hợp đi cùng quần âu, kaki, trang phục lịch sự.&nbsp;</li><li><strong>Màu</strong>: Nâu Cafe</li><li><strong>Kích thước: </strong>38 – 43</li><li>&nbsp;</li><li>Chất liệu da bò nhập khẩu 100% từ châu Âu.</li><li>Da được xử lý theo đúng quy trình nên sử dụng càng lâu thì giày sẽ càng mềm mại, dẻo dai, bền màu và tăng độ bóng mịn.&nbsp;</li><li>Dễ dàng vệ sinh và bảo quản.</li></ul><p>&gt;</p>', 1, 1),
(95, 1, 'Sản phẩm không bán', '200000000', '368502065untitled-1-01.jpg', '2022-10-15 10:04:14', '2022-10-15 10:04:14', '<p><strong>sản phẩm này rất đẹp</strong></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&gt;</p><p>&gt;</p><p>&gt;</p><p>&gt;</p><p>&gt;</p><p>&gt;</p>', 1, 1),
(96, 1, 'Hằng xinh gái', '2800000', '1665979704hang-xinh-gai.jpg', '2022-10-17 11:08:24', '2022-10-17 11:08:24', '<p>Hằng xinh gái</p>', 1, -1),
(97, 2, 'Doan Ngoc Hang', '123', '1665979889hang-xinh-gai.jpg', '2022-10-17 11:11:29', '2022-10-17 11:11:29', '<p>toàn đại ka</p>', 1, -1),
(98, 2, 'Đoàn Đắc Tùng', '28000', '1665979990hang-xinh-gai.jpg', '2022-10-17 11:13:10', '2022-10-17 11:13:10', '<p>123</p>', 1, -1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `title`) VALUES
(1, 'member'),
(10, 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `sizes`
--

INSERT INTO `sizes` (`id`, `size`) VALUES
(1, 38),
(2, 39),
(3, 40),
(4, 41),
(5, 42),
(6, 43);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `store`
--

CREATE TABLE `store` (
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `store`
--

INSERT INTO `store` (`product_id`, `quantity`, `size_id`, `created_at`) VALUES
(13, 8, 1, '2022-09-18 14:42:18'),
(13, 10, 2, '2022-09-18 14:42:18'),
(13, 10, 3, '2022-09-18 14:42:18'),
(13, 9, 4, '2022-09-18 14:42:18'),
(13, 10, 5, '2022-09-18 14:42:18'),
(13, 9, 6, '2022-09-18 14:42:18'),
(12, 10, 1, '2022-09-18 14:42:18'),
(12, 10, 2, '2022-09-18 14:42:18'),
(12, 10, 3, '2022-09-18 14:42:18'),
(12, 8, 4, '2022-09-18 14:42:18'),
(12, 10, 5, '2022-09-18 14:42:18'),
(12, 10, 6, '2022-09-18 14:42:18'),
(73, 10, 1, '2022-09-18 14:42:18'),
(73, 10, 2, '2022-09-18 14:42:18'),
(73, 10, 3, '2022-09-18 14:42:18'),
(73, 9, 4, '2022-09-18 14:42:18'),
(73, 9, 5, '2022-09-18 14:42:18'),
(73, 10, 6, '2022-09-18 14:42:18'),
(74, 10, 1, '2022-09-18 14:42:18'),
(74, 10, 2, '2022-09-18 14:42:18'),
(74, 10, 3, '2022-09-18 14:42:18'),
(74, 10, 4, '2022-09-18 14:42:18'),
(74, 10, 5, '2022-09-18 14:42:18'),
(74, 10, 6, '2022-09-18 14:42:18'),
(75, 10, 1, '2022-09-18 14:42:18'),
(75, 10, 2, '2022-09-18 14:42:18'),
(75, 10, 3, '2022-09-18 14:42:18'),
(75, 10, 4, '2022-09-18 14:42:18'),
(75, 10, 5, '2022-09-18 14:42:18'),
(75, 10, 6, '2022-09-18 14:42:18'),
(73, 10, 1, '2022-09-18 14:42:18'),
(73, 10, 2, '2022-09-18 14:42:18'),
(73, 10, 3, '2022-09-18 14:42:18'),
(73, 9, 4, '2022-09-18 14:42:18'),
(73, 9, 5, '2022-09-18 14:42:18'),
(74, 10, 1, '2022-09-18 14:42:18'),
(74, 10, 2, '2022-09-18 14:42:18'),
(74, 10, 3, '2022-09-18 14:42:18'),
(74, 10, 4, '2022-09-18 14:42:18'),
(74, 10, 5, '2022-09-18 14:42:18'),
(75, 10, 1, '2022-09-18 14:42:18'),
(73, 10, 2, '2022-09-18 14:42:18'),
(73, 10, 3, '2022-09-18 14:42:18'),
(73, 9, 4, '2022-09-18 14:42:18'),
(73, 9, 5, '2022-09-18 14:42:18'),
(74, 10, 1, '2022-09-18 14:42:18'),
(74, 10, 2, '2022-09-18 14:42:18'),
(74, 10, 3, '2022-09-18 14:42:18'),
(74, 10, 4, '2022-09-18 14:42:18'),
(74, 10, 5, '2022-09-18 14:42:18'),
(75, 10, 1, '2022-09-18 14:42:18'),
(75, 10, 2, '2022-09-18 14:42:18'),
(75, 10, 3, '2022-09-18 14:42:18'),
(75, 10, 4, '2022-09-18 14:42:18'),
(75, 10, 5, '2022-09-18 14:42:18'),
(77, 10, 1, '2022-09-18 14:42:18'),
(77, 10, 2, '2022-09-18 14:42:18'),
(77, 10, 3, '2022-09-18 14:42:18'),
(77, 9, 4, '2022-09-18 14:42:18'),
(77, 10, 5, '2022-09-18 14:42:18'),
(78, 10, 1, '2022-09-18 14:42:18'),
(78, 10, 2, '2022-09-18 14:42:18'),
(78, 10, 3, '2022-09-18 14:42:18'),
(78, 10, 4, '2022-09-18 14:42:18'),
(78, 10, 5, '2022-09-18 14:42:18'),
(79, 10, 1, '2022-09-18 14:42:18'),
(79, 10, 2, '2022-09-18 14:42:18'),
(79, 10, 3, '2022-09-18 14:42:18'),
(79, 10, 4, '2022-09-18 14:42:18'),
(79, 10, 5, '2022-09-18 14:42:18'),
(80, 10, 1, '2022-09-18 14:42:18'),
(80, 10, 2, '2022-09-18 14:42:18'),
(80, 10, 3, '2022-09-18 14:42:18'),
(80, 10, 4, '2022-09-18 14:42:18'),
(80, 10, 5, '2022-09-18 14:42:18'),
(81, 10, 1, '2022-09-18 14:42:18'),
(81, 10, 2, '2022-09-18 14:42:18'),
(81, 10, 3, '2022-09-18 14:42:18'),
(81, 10, 4, '2022-09-18 14:42:18'),
(81, 10, 5, '2022-09-18 14:42:18'),
(82, 10, 1, '2022-09-18 14:42:18'),
(82, 10, 2, '2022-09-18 14:42:18'),
(82, 10, 3, '2022-09-18 14:42:18'),
(82, 10, 4, '2022-09-18 14:42:18'),
(82, 10, 5, '2022-09-18 14:42:18'),
(83, 10, 1, '2022-09-18 14:42:18'),
(83, 9, 2, '2022-09-18 14:42:18'),
(83, 6, 6, '2022-09-18 14:42:18'),
(83, 10, 1, '2022-09-18 14:42:18'),
(83, 9, 2, '2022-09-18 14:42:18'),
(83, 6, 6, '2022-09-18 14:42:18'),
(84, 20, 2, '2022-09-19 00:37:09'),
(84, 20, 4, '2022-09-19 00:37:09'),
(84, 16, 5, '2022-09-19 00:37:09'),
(86, 10, 1, '2022-09-19 10:42:38'),
(86, 9, 2, '2022-09-19 10:42:38'),
(87, 20, 1, '2022-09-22 09:50:55'),
(87, 20, 2, '2022-09-22 09:50:56'),
(87, 20, 3, '2022-09-22 09:50:56'),
(87, 20, 4, '2022-09-22 09:50:56'),
(87, 20, 5, '2022-09-22 09:50:56'),
(87, 20, 6, '2022-09-22 09:50:56'),
(91, 20, 1, '2022-09-22 09:51:17'),
(91, 20, 2, '2022-09-22 09:51:17'),
(91, 20, 3, '2022-09-22 09:51:17'),
(91, 20, 4, '2022-09-22 09:51:17'),
(91, 15, 5, '2022-09-22 09:51:17'),
(91, 18, 6, '2022-09-22 09:51:17'),
(92, 20, 3, '2022-09-22 09:51:37'),
(92, 20, 4, '2022-09-22 09:51:37'),
(92, 20, 5, '2022-09-22 09:51:37'),
(92, 20, 6, '2022-09-22 09:51:37'),
(93, 20, 1, '2022-09-22 09:51:58'),
(93, 20, 2, '2022-09-22 09:51:58'),
(93, 20, 3, '2022-09-22 09:51:58'),
(93, 20, 4, '2022-09-22 09:51:58'),
(93, 19, 5, '2022-09-22 09:51:58'),
(93, 19, 6, '2022-09-22 09:51:58'),
(89, 20, 1, '2022-09-22 09:52:15'),
(89, 20, 2, '2022-09-22 09:52:15'),
(89, 20, 3, '2022-09-22 09:52:15'),
(89, 20, 4, '2022-09-22 09:52:15'),
(89, 20, 5, '2022-09-22 09:52:15'),
(89, 20, 6, '2022-09-22 09:52:15'),
(94, 20, 1, '2022-09-22 09:52:55'),
(94, 20, 2, '2022-09-22 09:52:55'),
(94, 20, 3, '2022-09-22 09:52:55'),
(94, 20, 4, '2022-09-22 09:52:55'),
(94, 20, 5, '2022-09-22 09:52:55'),
(94, 20, 6, '2022-09-22 09:52:55'),
(90, 10, 1, '2022-09-22 09:54:18'),
(90, 10, 2, '2022-09-22 09:54:18'),
(90, 10, 3, '2022-09-22 09:54:18'),
(90, 10, 4, '2022-09-22 09:54:18'),
(90, 10, 5, '2022-09-22 09:54:18'),
(90, 9, 6, '2022-09-22 09:54:18'),
(88, 10, 2, '2022-09-22 09:54:31'),
(88, 10, 3, '2022-09-22 09:54:31'),
(88, 10, 4, '2022-09-22 09:54:31'),
(88, 10, 5, '2022-09-22 09:54:31'),
(88, 10, 6, '2022-09-22 09:54:31'),
(93, 9, 5, '2022-09-22 09:57:57'),
(93, 9, 6, '2022-09-22 09:57:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'null',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `role_id` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `avatar`, `status`, `created_at`, `role_id`) VALUES
(3, 'toannd158@gmail.com', '4a3825c9247765c50463702745d4ede7', 'Doan Ngoc Toan', 'null', 1, '2022-09-19 23:55:18', 10),
(4, 'buihaison112@gmail.com', 'f4b63a54e53ddf6a92eafcb0dc512f1f', 'Bùi Hải Sơn', 'null', 1, '2022-09-30 21:31:35', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vnpay`
--

CREATE TABLE `vnpay` (
  `id` int(11) NOT NULL,
  `vnp_amount` varchar(50) NOT NULL,
  `vnp_bankcode` varchar(50) NOT NULL,
  `vnp_banktranno` varchar(50) NOT NULL,
  `vnp_cardtype` varchar(50) NOT NULL,
  `vnp_orderinfo` varchar(100) NOT NULL,
  `vnp_paydate` varchar(50) NOT NULL,
  `vnp_responsecode` varchar(50) NOT NULL,
  `vnp_tmncode` varchar(50) NOT NULL,
  `vnp_transactionno` varchar(50) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `vnpay`
--

INSERT INTO `vnpay` (`id`, `vnp_amount`, `vnp_bankcode`, `vnp_banktranno`, `vnp_cardtype`, `vnp_orderinfo`, `vnp_paydate`, `vnp_responsecode`, `vnp_tmncode`, `vnp_transactionno`, `order_id`) VALUES
(8, '145000000', 'NCB', 'VNP13841669', 'ATM', 'Thanh toán tại website', '20220924101343', '00', '9858U87X', '13841669', 1864),
(9, '145000000', 'NCB', 'VNP13841904', 'ATM', 'Thanh toán tại website', '20220925174447', '00', '9858U87X', '13841904', 8664),
(10, '145000000', 'NCB', 'VNP13842268', 'ATM', 'Thanh toán tại website', '20220926120030', '00', '9858U87X', '13842268', 5109),
(11, '185000000', 'NCB', 'VNP13842597', 'ATM', 'Thanh toán tại website', '20220926165247', '00', '9858U87X', '13842597', 1532),
(13, '290000000', 'NCB', 'VNP13842605', 'ATM', 'Thanh toán tại website', '20220926165442', '00', '9858U87X', '13842605', 1035),
(14, '170000000', 'NCB', 'VNP13842762', 'ATM', 'Thanh toán tại website', '20220926233837', '00', '9858U87X', '13842762', 9514),
(15, '160000000', 'NCB', 'VNP13842763', 'ATM', 'Thanh toán tại website', '20220926234003', '00', '9858U87X', '13842763', 9681),
(16, '185000000', 'NCB', 'VNP13846839', 'ATM', 'Thanh toán tại website', '20220930225847', '00', '9858U87X', '13846839', 7682);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bills`
--
ALTER TABLE `bills`
  ADD KEY `fk_bill_order` (`order_id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `feedback_img`
--
ALTER TABLE `feedback_img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_imgfb_fb` (`feedback_id`);

--
-- Chỉ mục cho bảng `list_image`
--
ALTER TABLE `list_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_listimage` (`product_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_order` (`customer_id`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orders_order_detail` (`order_id`),
  ADD KEY `fk_products_order_detail` (`product_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_category` (`category_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `size` (`size`);

--
-- Chỉ mục cho bảng `store`
--
ALTER TABLE `store`
  ADD KEY `fk_store_product` (`product_id`),
  ADD KEY `fk_store_size` (`size_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_user_role` (`role_id`);

--
-- Chỉ mục cho bảng `vnpay`
--
ALTER TABLE `vnpay`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `feedback_img`
--
ALTER TABLE `feedback_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `list_image`
--
ALTER TABLE `list_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT cho bảng `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `vnpay`
--
ALTER TABLE `vnpay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `fk_bill_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `feedback_img`
--
ALTER TABLE `feedback_img`
  ADD CONSTRAINT `fk_imgfb_fb` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`id`);

--
-- Các ràng buộc cho bảng `list_image`
--
ALTER TABLE `list_image`
  ADD CONSTRAINT `fk_product_listimage` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_customer_order` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `fk_orders_order_detail` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_products_order_detail` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `fk_store_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_store_size` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
